# Projet Explorateur de Fichier
Ce projet fait en Java et JavaFX est une application lourde créant une interface graphique pour naviguer dans les
dossiers.

Ce projet est incomplet et contient des bugs à résoudre.

## Technologies utilisées
* [JavaFX](https://fxdocs.github.io/docs/html5/) library Java permettant de réaliser des applications lourdes en utilisant du [FXML](https://openjfx.io/javadoc/21/javafx.fxml/javafx/fxml/doc-files/introduction_to_fxml.html) pour représenter l'affichage
* Java et la programmation orientée objet
* [JUnit5](https://junit.org/junit5/docs/current/user-guide/#overview) Pour faire et exécuter les tests

## Structure du projet
* [Modèle](src/main/java/co/simplon/promo27/model/) Les classes modèles ne sont pas liées à l'affichage et sont celles qui servent à déclencher des modifications/lister les fichiers/etc. On pourrait techniquement n'utiliser que ces classes et la ligne de commande sans l'interface graphique
* [Contrôleurs](src/main/java/co/simplon/promo27/controller/) Classes permettant de faire le lien entre la vue et le modèle, c'est elles qui indiquent à la vue quelles données afficher et qui déclenchent les méthodes du modèle pour appliquer des modifications
* [Composants](src/main/java/co/simplon/promo27/component/) Classes permettant de créer des balises FXML custom pour réutiliser des structures et logiques d'affichage et séparer le code en plusieurs parties
* [Vues](src/main/resources/co/simplon/promo27/) Dans ce projet, les vues sont représentées par des fichiers fxml (du XML en vrai, similaire dans sa syntaxe au HTML, mais les balises ne sont pas les mêmes)
* [Tests](src/test/java/co/simplon/promo27/) Classes de tests permettant de lancer un environnement de test pour exécuter et ré-exécuter le code (principalement le modèle ici) pour vérifier si celui ci fonctionne et continue à fonctionner au fil des modifications

## How To Use
1. Cloner le projet
2. Aller dans l'onglet tests et les exécuter pour voir si l'interaction avec les fichiers fonctionne
3. Lancer le projet avec F5 depuis un fichier .java ou via l'onglet maven > plugins > javafx > run

## Travail à faire
Par groupes de 3 personnes.

1. Une personne du groupe fera un fork de ce projet et assignera les autres personnes du groupe en member/maintainer du projet obtenu
2. Pour commencer, tenter de se familiariser avec la structure du projet en regardant la documentation de celui ci, le README et en essayant de modifier un peu l'interface. C'est un travail qui peut être fait en pair programming avec les différents membres du groupes.
3. Sur le projet formateur, aller dans l'onglet [Issues](https://gitlab.com/simplonlyon/promo27/project-explorer/-/issues) pour voir la listes des objectifs et tâches à réaliser sur le projet
4. En début d'après midi, faire un stand up meeting où chaque membre du groupe indique ce sur quoi il ou elle va travailler ou est en train de travailler et si il y a des blocages (sans détailler les blocages)
5. Pour chaque issue à traiter, créer une branche, faire vos modifications dessus, la push sur le dépôt et la fusionner sur la main après review par un ou les autres membres de l'équipe