module co.simplon.promo27 {
    requires javafx.controls;
    requires javafx.fxml;

    opens co.simplon.promo27 to javafx.fxml;
    opens co.simplon.promo27.controller to javafx.fxml;
    opens co.simplon.promo27.component to javafx.fxml;
    exports co.simplon.promo27;
    exports co.simplon.promo27.controller;
    exports co.simplon.promo27.component;
    exports co.simplon.promo27.model;

}
